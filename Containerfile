FROM docker.io/library/openjdk:11.0.8-jre

ARG SONAR_VERSION=4.5.0.2216
ARG SONAR_SCANNER_CLI=sonar-scanner-cli-${SONAR_VERSION}
ARG SONAR_SCANNER=sonar-scanner-${SONAR_VERSION}

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update -y && \
    apt-get install --no-install-recommends -y python3 nodejs curl && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

WORKDIR /bin
RUN curl https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/${SONAR_SCANNER_CLI}.zip -so /bin/${SONAR_SCANNER_CLI}.zip
RUN unzip ${SONAR_SCANNER_CLI}.zip \
    && rm ${SONAR_SCANNER_CLI}.zip

COPY drone-sonar /bin/

ENV PATH $PATH:/bin/${SONAR_SCANNER}/bin

ENTRYPOINT ["/bin/drone-sonar"]
