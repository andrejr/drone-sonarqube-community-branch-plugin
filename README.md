# drone-sonarqube-community-branch-plugin

A Drone plugin with support for Pull Request analysis (commercial or
[community-branch-plugin](https://github.com/mc1arke/sonarqube-community-branch-plugin))

This plugin was inspired by
[aosapps/drone-sonar-plugin](https://github.com/aosapps/drone-sonar-plugin),
but it adds support for branch and pull request analysis, either provided by
the developer edition (commercial) or by the opensource
[community-branch-plugin](https://github.com/mc1arke/sonarqube-community-branch-plugin)
which replicates that functionality.

## Examples

This example enables branch and pull request analysis:

```yaml
- name: code-analysis
  image: registry.gitlab.com/andrejr/drone-sonarqube-community-branch-plugin
  settings:
    sonar_host:
      from_secret: sonar_host
    sonar_token:
      from_secret: sonar_token
    branch_analysis: "true"
    target_branch: develop
```

Please note that your `sonar-project.properties` file mustn't contain any of
the `sonar.branch.*` or `sonar.pullrequest.*` settings.
These are dynamically inserted by this plugin, depending on whether the current
pipeline is a pull request or just a push.

## Secrets reference

##### sonar\_host

Host of SonarQube with schema (http/https).

##### sonar\_token

User token used to post the analysis report to SonarQube Server. Click *User –
My Account – Security – Generate Tokens*.

## Parameter reference

##### version
Code version, `$DRONE_COMMIT_SHA` by default.

##### timeout
Web request timeout in seconds, 60 by default.

##### sources
Comma delimited paths to directories containing source files.

##### inclusions
Comma delimited list of file path patterns to be included
 in analysis. When set, only files matching the paths set here will
be included in analysis.

##### log\_level

Control the quantity / level of logs produced during an 
analysis. Default value INFO. 

- `DEBUG`: Display `INFO` logs + more details at `DEBUG` level. 
- `TRACE`: Display `DEBUG` logs + the timings of all ElasticSearch queries and Web
  API calls executed by the SonarQube Scanner.

##### show\_profiling
Display logs to see where the analyzer spends time. False by default

##### target\_branch
Target branch for checking the feature branch against.
Used only for branch analysis, not for pull request analysis.
